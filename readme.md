# React Learning track

A simple React seed project built with ES6, Webpack and Babel. This has Hot Module replacement built in, provides unit testing using [Enzyme](http://airbnb.io/enzyme/), and accessibilty testing using [The A11y Machine](https://github.com/liip/TheA11yMachine).

This project intentionally leaves out `Flux` or `router` libraries, so that user can choose thier own implementations.

### Prerequisites

* [Node.js](https://nodejs.org/) - Ensure that Node.js above 4.x is installed.

This project doesn't require any global npm packages.

### Installations

Run `npm i` from within the project. It'll install all needed dependencies.

### Running the app

Simplest way to start the app is by issuing following command

* For development - `npm start`
    - Should be used while developing the app.
    - All files will be constantly watched and converted to appropritate format in the background.
* For production  - `npm run prod`
    - This will just output the results in `dist` folder which can be deployed as per the need.
    - The whole execution will happen only once.

When in development mode, open [http://localhost:3000](http://localhost:3000) in browser to see the app.

### Unit Testing the app

Run `npm test` to test the app. It'll additionally run coverage report for the app, which can be found in `reports/coverage` folder.

If you want to keep running tests ing background while adding/ correcting them, you can use following command -

`npm run test:watch`

To see coverage reports, run `npm run coverage` and then open [http://localhost:3100](http://localhost:3100) in the browser.

To run the test once and keep coverage reports running in background, use the following command -

`npm run build:test`

### Accessibility Testing

To test the app for any accessibilty defects, run the following command -

`npm run a11y`

This will generate accessibility reports in the folder `reports/accessibility`. To view this report either direct open the `index.html` file or run the accessibility dashboard using following command -

`npm run dashboard`

You can see the accessibility reports on [http://localhost:3200](http://localhost:3200)

If you want to run accessibility once and keep the dashboard running in the background, run the following command -

`npm run build:access`

### Complete reports

To run both accessibility and unit test once and their respective servers in background, use the following command -

`npm run build:reports`


### All commands

These are all commands available in this app. You can modify as per the need

* `npm test` - [DEFAULT TEST] Unit test the app once
* `npm run test:watch` - Run the unit test continuously
* `npm run a11y` - Run accessibility testing for the app once
* `npm run clean` - Clean the `dist` folder, before rebuilding
* `npm run copyhtml` - Copy the html files to dist
* `npm run copyassets` - Copy the assets to dist. Currently not being used in this seed project. Modify as per wish
* `npm run watch:js` - Use webpack to compile JS and watch in background. Currently not used in this seed project
* `npm run watch:css` - Compile `scss` file to `css` and watch for changes
* `npm run build:css` - Compile `scss` file to `css` once
* `npm run build:js` - Use webpack to compile minified, production ready JS
* `npm run server` - Run `webpack-dev-server` in the background while transpiling JS using webpack  
* `npm run build:dev` - Run  `watch:css` and `server` in parallel
* `npm run build:prod` - Run  `build:js` and `build:css` in parallel
* `npm start` - [DEFAULT APP] Default app build for development. Runs `clean`, `copyhtml`, `build:css`, and `build:dev` in series.
* `npm run prod` - Build app for production. Runs  `clean`, `copyhtml`, and `build:prod` in series
* `npm run dashboard` - Runs the accessibility dashboard and creates the server for report
* `npm run coverage` - Runs the code coverage report server
* `npm run build:test` - Run the test once and keep coverage reports running in background
* `npm run build:access` - Run accessibility once and keep the dashboard running in the background
* `npm run build:reports` - Runs both accessibility and unit test once and their respective servers in background



















