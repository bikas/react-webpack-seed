import React from 'react';
import {shallow, mount, render} from 'enzyme';
import jasmineEnzyme from 'jasmine-enzyme';
import Users from '../src/scripts/partials/users.jsx';
import User from '../src/scripts/partials/user.jsx';

describe('Users', () => {
	let userObj = {
		picture: {
			large: ''
		},
		name: {
			title: 'Mr.',
			first: 'fname',
			last: 'lname'
		},
		gender: 'Male',
		email: 'abc@xyz.com',
		cell: '(123) 456 7890'
	};

	let usersArr = [userObj, userObj, userObj];

	beforeEach(() => {
		jasmineEnzyme();
	});

	let shallowComp = shallow(<Users users={usersArr} />);
	let mountComp = mount(<Users users={usersArr} />);

	it('renders the component', () => {
		expect(shallowComp).toBeTruthy();
	});

	it('should have Users react component', () => {
		expect(mountComp).toContainReact(<Users users={usersArr} />);
	});

	it('should render 3 child components', () => {
		expect(mountComp.find(User).length).toEqual(3);
	});

	it('should render no child component if empty array is provided', () => {
		let noChildComp = mount(<Users users={[]} />);
		expect(noChildComp.find(User).length).toEqual(0);
	});
});