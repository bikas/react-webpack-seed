import React from 'react';
import {shallow, mount, render} from 'enzyme';
import jasmineEnzyme from 'jasmine-enzyme';
import Dashboard from '../src/scripts/components/dashboard.jsx';
import Users from '../src/scripts/partials/users.jsx';

describe('Dashboard', () => {
	let userObj = {
		picture: {
			large: ''
		},
		name: {
			title: 'Mr.',
			first: 'fname',
			last: 'lname'
		},
		gender: 'Male',
		email: 'abc@xyz.com',
		cell: '(123) 456 7890'
	};

	let usersArr = [userObj, userObj, userObj];

	beforeEach(() => {
		jasmineEnzyme();
	});

	let shallowComp = shallow(<Dashboard users={usersArr} />);
	let mountComp = mount(<Dashboard users={usersArr} />);

	it('renders the component', () => {
		expect(shallowComp).toBeTruthy();
	});

	it('should have Dashboard react component', () => {
		expect(mountComp).toContainReact(<Dashboard users={usersArr} />);
	});

	it('should render 1 immediate child components', () => {
		expect(mountComp.find(Users).length).toEqual(1);
	});

	it('should render no child components', () => {
		let noChildComp = mount(<Dashboard />);
		expect(noChildComp.find(Users).length).toEqual(0);
	});
});