import React from 'react';
import {shallow, mount, render} from 'enzyme';
import jasmineEnzyme from 'jasmine-enzyme';
import Header from '../src/scripts/partials/header.jsx';

describe('Header', () => {
	beforeEach(() => {
		jasmineEnzyme();
	});

	let shallowComp = shallow(<Header title="Test Title" />);
	let mountComp = mount(<Header title="Test Title" />);
	let shallowEmptyComp = shallow(<Header />);

	it('renders the component', () => {
		expect(shallowComp).toBeTruthy();
	});

	it('should have Header react component', () => {
		expect(mountComp).toContainReact(<Header title="Test Title" />);
	});

	it('should contain the passed title', () => {
		let value = mountComp.find('.navbar-brand').text();
		expect(value).toEqual('Test Title');
	});

	it('should contain no title if props is not passed', () => {
		let value = shallowEmptyComp.find('.navbar-brand').text();
		expect(value).toEqual('');
	});
});