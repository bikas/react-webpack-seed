import React from 'react';
import {shallow, mount, render} from 'enzyme';
import jasmineEnzyme from 'jasmine-enzyme';
import User from '../src/scripts/partials/user.jsx';

describe('User', () => {
	let userObj = {
		picture: {
			large: ''
		},
		name: {
			title: 'Mr.',
			first: 'fname',
			last: 'lname'
		},
		gender: 'Male',
		email: 'abc@xyz.com',
		cell: '(123) 456 7890'
	};

	beforeEach(() => {
		jasmineEnzyme();
	});

	let shallowComp = shallow(<User user={userObj} />);
	let mountComp = mount(<User user={userObj} />);

	it('renders the component', () => {
		expect(shallowComp).toBeTruthy();
	});

	it('should have User react component', () => {
		expect(mountComp).toContainReact(<User user={userObj} />);
	});

	it('should contain a div with class card', () => {
		expect(mountComp.find('.card').length).toEqual(1);
	});

	it('should generate full name with capitalization', () => {
		let value = mountComp.find('.card-title').text();
		expect(value).toEqual('Mr. Fname Lname');
	});

	it('should generate gender of the user', () => {
		let value = mountComp.find('.list-group-item').get(0).textContent;
		expect(value).toEqual('Male');
	});

	it('should generate email of the user', () => {
		let value = mountComp.find('.list-group-item').get(1).textContent;
		expect(value).toEqual('abc@xyz.com');
	});

	it('should generate cell of the user', () => {
		let value = mountComp.find('.list-group-item').get(2).textContent;
		expect(value).toEqual('(123) 456 7890');
	});
});