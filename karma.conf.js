var path = require('path');

module.exports = function(config) {
	config.set({
		basePath: '',
		frameworks: ['jasmine'],
		files: [
			'tests/**/*.jsx',
			'tests/**/*.js'
		],

		preprocessors: {
			'src/**/*.jsx': ['webpack', 'sourcemap'],
			'src/**/*.js': ['webpack', 'sourcemap'],
			'tests/**/*.jsx': ['webpack', 'sourcemap'],
			'tests/**/*.js': ['webpack', 'sourcemap']
		},

		webpack: {
			devtool: 'inline-source-map',
			module: {
				loaders: [
					{
						test: /\.(js|jsx)$/,
						loader: 'babel',
						exclude: /(node_modules|bower_components)\//,
						query: {
							presets: ['airbnb']
						}
					},
					{
						test: /\.json$/,
						loader: 'json'
					},
				],
				postLoaders: [
					{
						test: /\.(js|jsx)$/,
						exclude: /(tests|node_modules|bower_components)\//,
						loader: 'istanbul-instrumenter'
					}
				]
			},
			externals: {
				'react/lib/ExecutionEnvironment': true,
				'react/lib/ReactContext': true
			}
		},

		webpackServer: {
			noInfo: true
		},

		coverageReporter: {
			type: 'html',
			dir: 'reports/',
			subdir: 'coverage'
		},

		plugins: [
			'karma-webpack',
			'karma-jasmine',
			'karma-sourcemap-loader',
			'karma-chrome-launcher',
			'karma-phantomjs-launcher',
			'karma-spec-reporter',
			'karma-coverage'
		],

		babelPreprocessor: {
			options: {
				presets: ['airbnb']
			}
		},

		reporters: ['spec', 'coverage'],
		port: 9876,
		colors: true,
		logLevel: config.LOG_INFO,
		autoWatch: true,
		browsers: ['Chrome'],
		singleRun: false,
	});
};