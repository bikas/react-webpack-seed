var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'dist');
var APP_DIR = path.resolve(__dirname, 'src');

var babelQuery = {
	presets: [
		'latest',
		'react'
	],
	plugins: [
		'transform-react-constant-elements',
		'transform-react-inline-elements',
		'transform-es3-member-expression-literals',
		'transform-es3-property-literals'
	]
};

var config = {
	entry: APP_DIR + '/index.jsx',

	output: {
		path: BUILD_DIR,
		filename: 'bundle.js'
	},

	devtool: null,

	module : {
		loaders : [{
			test : /\.(js|jsx)?/,
			exclude: /(tests|dist)/,
			include : [
				APP_DIR,
				/node_modules\/ajaxed-promise\/ajaxed-promise.js/  // imported es6 module which needs to be converted
			],
			loaders: [
				'es3ify',
				`babel?${JSON.stringify(babelQuery)}`
			]
		}, {
			test: /\.json$/,
			loader: 'json'
		}]
	},

	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('production')
			}
		})
	]
};

module.exports = config;