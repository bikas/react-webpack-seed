import React from 'react';
import Dashboard from './components/dashboard.jsx';
import ajax from 'ajaxed-promise';

export default class App extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			users: []
		};
	}

	componentWillMount() {
		let _this = this;

		ajax('https://randomuser.me/api/?results=12')
			.get()
			.then(function(data) {
				if(typeof data === 'string') {
					data = JSON.parse(data);
				}
				
				_this.setState({
					users: data.results
				});
			});
	}

	render() {
		return (
			<Dashboard
				users={this.state.users}
			/> 
		);
	}
}