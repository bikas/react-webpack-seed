import React from 'react';
import Header from '../partials/header.jsx';
import Users from '../partials/users.jsx';

const defaultProps = {
	users: []
};

export default class Dashboard extends React.Component {
	constructor(props) {
		super(props);
	}

	renderUsers() {
		if(this.props.users.length > 0) {
			return <Users users={this.props.users} />;
		}
	}

	render() {
		return (
			<div>
				<Header title="React Webpack Seed" />

				{this.renderUsers()}
			</div>
		);
	}
}

Dashboard.defaultProps = defaultProps;