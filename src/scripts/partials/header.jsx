import React from 'react';

const propTypes = {
	title: React.PropTypes.string.isRequired
};
const defaultProps = {
	title: ''
};

export default class Header extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<nav className="navbar navbar-fixed-top navbar-dark bg-primary">
				<a className="navbar-brand" id="title">{this.props.title}</a>
			</nav>
		);
	}
}

Header.propTypes = propTypes;
Header.defaultProps = defaultProps;