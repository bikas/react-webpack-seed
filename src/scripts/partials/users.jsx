import React from 'react';
import User from './user.jsx';

const propTypes = {
	users: React.PropTypes.array.isRequired
};

const defaultProps = {
	users: []
};

export default class Users extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="container-fluid dashbaord">
				<div className="row">
					{this.props.users.map(function(user, index) {
						return <User key={index} user={user} />;
					})}
				</div>
			</div>
		);
	}
}

Users.propTypes = propTypes;
Users.defaultProps = defaultProps;