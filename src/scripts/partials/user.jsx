import React from 'react';

const propTypes = {
	user: React.PropTypes.object.isRequired
};

export default class User extends React.Component {
	constructor(props) {
		super(props);

		this.cardbg = [
			'card-info',
			'card-primary',
			'card-success',
			'card-warning',
			'card-danger'
		];
	}

	capitalize(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}

	getRandomClass() {
		let length = this.cardbg.length;
		let key = Math.floor(Math.random() * length);

		return this.cardbg[key];
	}

	render() {
		let user = this.props.user;

		return (
			<div className="col-sm-3">
				<div className={`card card-inverse ${this.getRandomClass()} text-xs-center`}>
					<img src={user.picture.large} alt="User Image" />

					<div className="card-block">
						<h4 className="card-title">
							{this.capitalize(user.name.title)} {this.capitalize(user.name.first)} {this.capitalize(user.name.last)}
						</h4>
					</div>

					<ul className="list-group list-group-flush">
						<li className="list-group-item"><b>{this.capitalize(user.gender)}</b></li>
						<li className="list-group-item"><a href={`mailto:${user.email}`}>{user.email}</a></li>
						<li className="list-group-item"><a href={`tel:${user.cell}`}>{user.cell}</a></li>
					</ul>
				</div>
			</div>
		);
	}
}

User.propTypes = propTypes;